package doko.model

import org.scalacheck._
import Prop._

object Versuch extends Properties("Versuch") {
    
    property("ok") = forAll((i: Int) => i == i)
    //property("hello") = forAll((s: String) => (s.size > 0) ==> (s.size == 1) :| "'" + s + "' hat  nicht Länge 1")
}