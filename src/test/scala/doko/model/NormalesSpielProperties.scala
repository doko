package doko.model

import org.scalacheck._
import Prop._
import Gen._

object NormalesSpielProperties extends Properties("Karten und Regeln normales Spiel") {
    
    val kartenGen: Gen[Karte] = for {
	f <- oneOf(Farbe.values.toSeq)
	w <- oneOf(Wert.values.toSeq)
	r <- oneOf(Rueckseite.values.toSeq)
    } yield Karte(f, w, r)
    
    property("Regeln vollständig") = forAll(kartenGen, kartenGen) { (erste: Karte, zweite: Karte) =>
    	try {
    	    NormalesSpiel.regeln(erste, zweite)
    	    true
    	} catch {
    	    case _ => false
    	}
    }
}