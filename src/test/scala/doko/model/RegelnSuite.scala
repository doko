package doko.model

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers

class RegelnSuite extends FunSuite with ShouldMatchers {
    
    val KaroNeunRot = Karte(Farbe.Karo, Wert.Neun, Rueckseite.Rot)
    val KaroNeunBlau = Karte(Farbe.Karo, Wert.Neun, Rueckseite.Blau)

    test("Erste zündende Regel definiert das Ergebnis") {
	val r = new Regeln(Seq(
		(_, _) => None,
		(_, _) => Some(Ergebnis.Bedient),
		(_, _) => Some(Ergebnis.Sticht)))
	r(KaroNeunRot, KaroNeunBlau) should be (Ergebnis.Bedient)
    }
    
    test("Keine zündende Regel erzeugt Fehler") {
	val r = new Regeln(Seq())
	evaluating {
	    r(KaroNeunRot, KaroNeunBlau)
	} should produce [RuntimeException]
    }
    
    test("Nachgelagerte Regeln werden nicht ausgewertet") {
	val r = new Regeln(Seq(
		(_, _) => None,
		(_, _) => Some(Ergebnis.Bedient),
		(_, _) => error("sollte nicht ausgewertet werden")))
	r(KaroNeunRot, KaroNeunBlau) should be (Ergebnis.Bedient)
    }
}