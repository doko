package doko.model

import org.scalatest.FunSuite
import org.scalatest.matchers.ShouldMatchers

class NormalesSpielSuite extends FunSuite with ShouldMatchers {

    test("Jedes Kartenpaar muss von einer Regel abgedeckt sein") {
	def alleKarten = for {
	    f <- Farbe.values
	    w <- Wert.values
	    r <- Rueckseite.values
	} yield Karte(f, w, r)
	
	for {
	    erste <- alleKarten
	    zweite <- alleKarten
	} {
	    NormalesSpiel.regeln(erste, zweite)
	}
    }
}