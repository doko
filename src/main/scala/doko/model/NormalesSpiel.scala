package doko.model

import scala.collection._

object NormalesSpiel {

 // aufsteigend sortierte Seq der Trümpfe (Farbe, Wert)
  val alleTruempfe = {
    import Wert._
    import Farbe._
    val b = immutable.Seq.newBuilder[(Farbe.Value, Wert.Value)]
    b ++= (for {
      wert <- Seq(Neun, Koenig, Zehn, Ass)
    } yield (Karo, wert))
    b ++= (for {
      wert <- Seq(Bube, Dame)
      farbe <- Seq(Karo, Herz, Pik, Kreuz)
    } yield (farbe, wert))
    b += ((Herz, Zehn))
    b.result
  }

  val regeln = new Regeln(Seq(
    trumpfStichtFarbe _,
    zweiteHerzZehnGewinnt _,
    hoehererTrumpfGewinnt _,
    hoeherGewinnt _,
    abwerfen _,
    ersteKarteGewinnt _
  ))

  def trumpf(karte: Karte): Boolean = alleTruempfe.contains(karte.rang)

  def trumpfStichtFarbe(erste: Karte, zweite: Karte): Option[Ergebnis.Value] =
    if (!trumpf(erste) && trumpf(zweite)) Some(Ergebnis.Sticht) else None

  def zweiteHerzZehnGewinnt(erste: Karte, zweite: Karte) = (erste, zweite) match {
    case (Karte(Farbe.Herz, Wert.Zehn, _), Karte(Farbe.Herz, Wert.Zehn, _)) => Some(Ergebnis.Ueberbietet)
    case _ => None
  }

  def hoehererTrumpfGewinnt(erste: Karte, zweite: Karte) = {
    val i1 = alleTruempfe.indexOf(erste)
    if(i1 >= 0) {
      val i2 = alleTruempfe.indexOf(zweite)
      if (i2 >= 0) {
	if (i1 < i2) Some(Ergebnis.Sticht)
	else Some(Ergebnis.Bedient)
      } else None
    } else None
  }

  def hoeherGewinnt(erste: Karte, zweite: Karte) =
    if (erste.farbe == zweite.farbe) {
      if (erste.wert < zweite.wert) Some(Ergebnis.Ueberbietet)
      else Some(Ergebnis.Bedient)
    }
    else None

  def abwerfen(erste: Karte, zweite: Karte) =
    if (erste.farbe != zweite.farbe) Some(Ergebnis.WirftAb)
    else None

  def ersteKarteGewinnt(erste: Karte, zweite: Karte) =
    if (erste.rang == zweite.rang) Some(Ergebnis.Bedient) else None
}
