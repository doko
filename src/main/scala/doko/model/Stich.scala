package doko.model

import scala.collection.immutable.List

case class Ausspiel(karte: Karte, spieler: Spieler)

case class Stich(bisher: List[Ausspiel], regeln: Regeln) {

    def legen(karte: Karte, spieler: Spieler): Stich =
	if (!offen) error("Stich ist schon vollendet.")
	else copy(bisher = Ausspiel(karte, spieler) :: bisher)

    def offen: Boolean = bisher.size < 4

    def gewinner: Spieler = bisher.reduceRight((vorhand, hinterhand) =>
	if (regeln.zweiterGewinnt(vorhand.karte, hinterhand.karte)) hinterhand else vorhand).spieler
}

case class Spiel(bisher: List[Stich], regeln: Regeln) {
    
    def legen(karte: Karte, spieler: Spieler): Spiel =
	if (bisher.head.offen) copy(bisher = bisher.head.legen(karte, spieler) :: bisher.tail)
	else if (bisher.head.gewinner != spieler) error("Falscher Spieler hat ausgespielt.")
	else copy(bisher = Stich(List(Ausspiel(karte, spieler)), regeln) :: bisher)
}

// Hier weiter