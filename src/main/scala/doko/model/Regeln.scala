package doko.model

import scala.collection._

object Ergebnis extends Enumeration {
    val WirftAb, Bedient, Ueberbietet, Sticht = Value
    def gewinnt(ergebnis: Value) = ergebnis.id > 1
}

object Regeln {
    type Regel = (Karte, Karte) => Option[Ergebnis.Value]
}

class Regeln(regeln: Seq[Regeln.Regel]) extends ((Karte, Karte) => Ergebnis.Value) {

    def apply(erste: Karte, zweite: Karte): Ergebnis.Value =
	regeln.iterator.map(_(erste, zweite)).find(_.isDefined) match {
	case Some(Some(ergebnis)) => ergebnis
	case _ => error("rules did not apply to " + (erste, zweite))
    }
    
    def zweiterGewinnt(erste: Karte, zweite: Karte) =
	Ergebnis.gewinnt(this(erste, zweite))
}
