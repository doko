package doko.model

object Farbe extends Enumeration {
  val Kreuz, Pik, Herz, Karo = Value
}

object Wert extends Enumeration {
  val Neun, Bube, Dame, Koenig, Zehn, Ass = Value
}

object Rueckseite extends Enumeration {
  val Rot, Blau = Value
}

case class Karte(farbe: Farbe.Value, wert: Wert.Value, rueckseite: Rueckseite.Value) {
  def rang = (farbe, wert)
}
