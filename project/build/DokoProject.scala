import sbt._

class DokoProject(info: ProjectInfo) extends DefaultProject(info) {
    
    lazy val hi = task { println("hello"); None }
    
    val scalaToolsSnapshots = ScalaToolsSnapshots
    
    val scalaCheck = "org.scala-tools.testing" % "scalacheck_2.8.0.RC3" % "1.7" withSources() withJavadoc()
    
    val scalaTest = "org.scalatest" % "scalatest" % "1.2-for-scala-2.8.0.RC3-SNAPSHOT" withSources() withJavadoc()
    
}